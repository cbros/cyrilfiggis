name := """cyrilfiggis"""

version := "1.0-SNAPSHOT"

play.Project.playScalaSettings

resolvers += "Neo4j Scala Repo" at "http://m2.neo4j.org/content/repositories/releases"

libraryDependencies += "eu.fakod" % "neo4j-scala_2.10" % "0.3.0" % "compile"
