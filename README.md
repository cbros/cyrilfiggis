# cyril figgis

## Build / Development Environment

### Prerequisites

+  Install `sbt`, put in `PATH` (http://www.scala-sbt.org/)

+  Install a JDK and put in `PATH` (I used the Oracle JDK 8 downloadable here: http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

### Generate Eclipse project files

        sbt
        [cyrilfiggis] $ eclipse with-sources=true

### Run hot-reloadable environment

        sbt
        play
        run
